#!/bin/bash

# Get the ID of the last created image
last_image=$(docker images -q | head -n 1)

# Get a list of all image IDs
all_images=$(docker images -q)

# Loop through the list of images and delete all except the last
for image in $all_images; do
  if [ $image != $last_image ]; then
    docker rmi $image
  fi
done
